import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class UserDiagramsPage extends PageBase {

    private final By logoutLocator = By.xpath("//a[@href='/users/sign_out']");
    private final By deleteDiagramLocator = By.xpath("//a[contains(., 'Delete')]");
    
    UserDiagramsPage(WebDriver driver) {
        super(driver);
    }

    public MainPage logout(String username) {
        waitAndReturnElement(By.xpath("//a[contains(., '" + username + "')]")).click();
        waitAndReturnElement(logoutLocator).click();
        waitForPageLoad(10000);
        return new MainPage(driver);
    }

    public void deleteDiagram() {
        waitAndReturnElement(deleteDiagramLocator).click();
        driver.switchTo().alert().accept();
    }
}
