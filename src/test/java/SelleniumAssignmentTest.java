import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.*;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import io.github.bonigarcia.wdm.WebDriverManager;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.List;
import java.util.Arrays;


public class SelleniumAssignmentTest {

    private WebDriver driver;

    private String username;
    private String userCompName;
    private String email;
    private String passwd;
    private List<String> staticPageLinks;
    private List<String> staticPageTexts;

    @Before
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        Properties prop = new Properties();
        try {
            prop.load(new FileInputStream("test.conf"));
        } catch (Exception e) {
            assert(false);
        }

        username = prop.getProperty("testuser.username");
        userCompName = prop.getProperty("testuser.userCompName");
        email = prop.getProperty("testuser.email");
        passwd = prop.getProperty("testuser.passwd");

        staticPageLinks = Arrays.asList(prop.getProperty("staticpages.links").split("\\\\"));
        staticPageTexts = Arrays.asList(prop.getProperty("staticpages.texts").split("\\\\"));
        assert(staticPageLinks.size() == staticPageTexts.size());
    }

    @Test
    public void loginLogoutTest() {
        MainPage mainPage = MainPage.create(driver);
        assert(mainPage.getBodyText().contains("Sign up") && mainPage.getBodyText().contains("Log in"));

        LoginPage loginPage = mainPage.getLoginPage();
        UserDiagramsPage userPage = loginPage.login(email, passwd);
        assert(userPage.getBodyText().contains(username));

        MainPage loggedoutPage = userPage.logout(username);
        assert(loggedoutPage.getBodyText().contains("Sign up") && mainPage.getBodyText().contains("Log in"));
    }

    @Test
    public void diagramTest() {
        MainPage mainPage = MainPage.create(driver);
        DiagramPage diagramPage = mainPage.getDiagramPage();
        diagramPage.setDiagramText("[A]^[B]");
        diagramPage.setDiagramStyle("Plain");
        SVGDiagramPage svgPage = diagramPage.getSVGDiagram();
        String diagramSVG = svgPage.getSVG();
        assert(diagramSVG.contains("<title>A</title>") && diagramSVG.contains("<title>B</title>") && diagramSVG.contains("<title>A-&gt;B</title>"));
    }

    @Test
    public void buyMeACoffeeTest() {
        MainPage mainPage = MainPage.create(driver);
        DiagramPage diagramPage = mainPage.getDiagramPage();
        BuyMeACoffeePage buyMeACoffeePage = diagramPage.getBuyMeACoffeePage();
        assertEquals("yUML is a tool to help developers create UML diagrams fast", buyMeACoffeePage.getTitle());
        assertEquals("1", buyMeACoffeePage.getCoffeeCountRadioValue());
    }

    @Test
    public void createAndDeleteUserDiagramTest() {
        String diagramName = RandomStringUtils.random(10, true, true);

        MainPage mainPage = MainPage.create(driver);
        LoginPage loginPage = mainPage.getLoginPage();
        UserDiagramsPage userPage = loginPage.login(email, passwd);
        DiagramPage diagramPage = userPage.getDiagramPage();
        diagramPage.addToMyDiagrams(diagramName);
        UserDiagramsPage userDiagramsPage = diagramPage.getUserDiagramsPage(userCompName);
        assert(userDiagramsPage.getBodyText().contains(diagramName));
        userDiagramsPage.deleteDiagram();
        assertFalse(userDiagramsPage.getBodyText().contains(diagramName));
    }

    @Test
    public void arrayPageAndHistoryTest() {
        GeneralPage page = MainPage.createGeneral(driver);
        for (int i = 0; i < staticPageLinks.size(); ++i) {
            assertTrue("On main page", page.getBodyText().contains("Create diagrams quickly"));
            page = page.redirectOnNavbarTo(staticPageLinks.get(i));
            assertTrue(staticPageLinks.get(i) + " contains \"" + staticPageTexts.get(i) + "\"", page.getBodyText().contains(staticPageTexts.get(i)));
            page = page.redirectBack();
            assertTrue("Back to the main page", page.getBodyText().contains("Create diagrams quickly"));
        }
    }
    
    @After
    public void close() {
        if (driver != null) {
            driver.quit();
        }
    }
}
