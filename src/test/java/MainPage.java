import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class MainPage extends PageBase {
    
    private final By loginLocator = By.xpath("//a[@href='/users/sign_in']");

    public static MainPage create(WebDriver driver) {
        driver.get("https://yuml.me/");
        return new MainPage(driver);
    }

    public static GeneralPage createGeneral(WebDriver driver) {
        driver.get("https://yuml.me/");
        return new GeneralPage(driver);
    }

    MainPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage getLoginPage() {
        waitAndReturnElement(loginLocator).click();
        waitForPageLoad(10000);
        return new LoginPage(driver);
    }
}
