import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

import java.util.List;

public class BuyMeACoffeePage extends PageBase {

    private final By coffeCountLocator = By.xpath("//input[@name='coffee-default-count']");
    private final By parentLocator = By.xpath("./..");
    
    BuyMeACoffeePage(WebDriver driver) {
        super(driver);
    }

    public String getCoffeeCountRadioValue() {
        List<WebElement> radios = driver.findElements(coffeCountLocator);
        for (WebElement radio : radios) {
            if (radio.findElement(parentLocator).getAttribute("class").contains("coffee-count-chosen")) {
                return radio.getAttribute("value");
            }
        }
        return "";
    }


}
