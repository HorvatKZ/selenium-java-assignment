import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

public class LoginPage extends PageBase {

    private final By emailInputLocator = By.xpath("//form[@id='new_user']/div/input[@id='user_email']");
    private final By passwdInputLocator = By.xpath("//form[@id='new_user']/div/input[@id='user_password']");
    private final By submitButtonLocator = By.xpath("//form[@id='new_user']//input[@type='submit']");
    
    LoginPage(WebDriver driver) {
        super(driver);
    }

    public UserDiagramsPage login(String email, String passwd) {
        waitAndReturnElement(emailInputLocator).sendKeys(email);
        waitAndReturnElement(passwdInputLocator).sendKeys(passwd);
        waitAndReturnElement(submitButtonLocator).click();
        waitForPageLoad(10000);
        return new UserDiagramsPage(driver);
    }
}
