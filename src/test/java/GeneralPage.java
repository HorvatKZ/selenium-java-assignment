import org.openqa.selenium.WebDriver;

public class GeneralPage extends PageBase {

    GeneralPage(WebDriver driver) {
        super(driver);
    }

    public GeneralPage redirectBack() {
        driver.navigate().back();
        waitForPageLoad(10000);
        return new GeneralPage(driver);
    }

}
