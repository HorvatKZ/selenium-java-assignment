import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;	

public class DiagramPage extends PageBase {
    
    private final By textAreaLocator = By.xpath("//textarea");
    private final By styleSelectorLocator = By.xpath("//select[@id='select-diagram-style']");
    private final By svgSelector = By.xpath("//a[@class='button is-small svg_link_icon']");
    private final By buyMeACoffeeSelector = By.xpath("//a[@href='https://www.buymeacoffee.com/yuml']");
    private final By addDiagramSelector = By.xpath("//button[@id='add-to-my-diagrams-button']");
    private final By diagramNameSelector = By.xpath("//input[@id='diagramName']");
    private final By saveDiagramSelector = By.xpath("//button[@id='saveBtn']");

    DiagramPage(WebDriver driver) {
        super(driver);
    }

    public void setDiagramText(String diagram) {
        WebElement textArea = waitAndReturnElement(textAreaLocator);
        textArea.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        textArea.sendKeys(diagram + "\n");
        waitForDiagramRegeneration();
    }

    public void setDiagramStyle(String style) {
        Select styleSelector = new Select(waitAndReturnElement(styleSelectorLocator));
        styleSelector.selectByVisibleText(style);
        waitForDiagramRegeneration();
    }

    public SVGDiagramPage getSVGDiagram() {
        waitAndReturnElement(svgSelector).click();
        switchToNextTab();
        return new SVGDiagramPage(driver);
    }

    public BuyMeACoffeePage getBuyMeACoffeePage() {
        waitAndReturnElement(buyMeACoffeeSelector).click();
        switchToNextTab();
        waitForPageLoad(10000);
        return new BuyMeACoffeePage(driver);
    }

    public UserDiagramsPage getUserDiagramsPage(String userCompName) {
        waitForPageLoad(10000);
        waitAndReturnElement(By.xpath("//a[@href='/" + userCompName + "/diagrams']")).click();
        return new UserDiagramsPage(driver);
    }

    public void addToMyDiagrams(String name) {
        waitAndReturnElement(addDiagramSelector).click();
        waitAndReturnElement(diagramNameSelector).sendKeys(name);
        waitAndReturnElement(saveDiagramSelector).click();
        waitStatic(3000); // Wait for saving diagram
    }

    private void switchToNextTab() {
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
    }

    private void waitForDiagramRegeneration() {
        waitStatic(2000);
    }
}
