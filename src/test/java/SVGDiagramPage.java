import org.openqa.selenium.WebDriver;

public class SVGDiagramPage extends PageBase {

    SVGDiagramPage(WebDriver driver) {
        super(driver);
    }

    public String getSVG() {
        return driver.getPageSource();
    }
}
