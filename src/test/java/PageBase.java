import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;	


class PageBase {
    protected WebDriver driver;
    protected WebDriverWait wait;
    
    protected final By bodyLocator = By.tagName("body");
    protected final By drawLocator = By.xpath("//a[@href='/diagram/scruffy/class/draw']");
    
    protected PageBase(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }
    
    protected WebElement waitAndReturnElement(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    protected void waitStatic(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {}
    }

    protected boolean waitForPageLoad(int limit) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        int waitTime = 0;
        int waitStep = 100;
        while (waitTime <= limit && !jse.executeScript("return document.readyState").toString().equals("complete")) {
            waitStatic(waitStep);
            waitTime += waitStep;
        }
        return waitTime <= limit;
    }

    public String getTitle() {
        return driver.getTitle();
    }
    
    public String getBodyText() {
        WebElement bodyElement = waitAndReturnElement(bodyLocator);
        return bodyElement.getText();
    }

    public DiagramPage getDiagramPage() {
        waitAndReturnElement(drawLocator).click();
        waitForPageLoad(10000);
        return new DiagramPage(driver);
    }
   
    public GeneralPage redirectOnNavbarTo(String href) {
        waitAndReturnElement(By.xpath("//a[@href='" + href + "']")).click();
        waitForPageLoad(10000);
        return new GeneralPage(driver);
    }
}
